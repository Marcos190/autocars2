import { useState } from 'react';

function About() {
  const [options, setOptions] = useState([]); // État local pour stocker les options du champ de sélection
  const [inputValue, setInputValue] = useState(''); // État local pour stocker la valeur de l'input

  const handleInputChange = (e) => {
    setInputValue(e.target.value); // Met à jour la valeur de l'input lorsqu'elle change
  };

  const handleAddOption = () => {
    if (inputValue.trim() !== '') {
      setOptions([...options, inputValue]); // Ajoute la valeur de l'input à la liste des options
      setInputValue(''); // Réinitialise la valeur de l'input
    }
  };

  return (
    <div className="flex items-center">
      <label className="mr-2">Choisissez une option :</label>
      <select className="border border-gray-300 rounded px-2 py-1">
        {options.map((option, index) => (
          <option key={index} value={option}>
            {option}
          </option>
        ))}
      </select>
      <input
        type="text"
        className="border border-gray-300 rounded px-2 py-1 ml-2"
        value={inputValue}
        onChange={handleInputChange}
      />
      <button
        className="bg-blue-500 text-white rounded px-2 py-1 ml-2"
        onClick={handleAddOption}
      >
        Ajouter
      </button>
    </div>
  );
}

export default About;
