import { useState } from 'react';
import img from '../assets/fond1.png';
import marque from '../assets/marque.png';
import '../styles/Forms.css';

function Form() {
    const [vehicles, setVehicles] = useState([]);
    const [formData, setFormData] = useState({
        brand: '',
        model: '',
        plaque: '',
        year: '',
        category: '',
        dateAdded: '',
    });
    
    const handleChange = (e) => {
        const { name, value } = e.target;
        setFormData((prevFormData) => ({
            ...prevFormData,
            [name]: value,
        }));
    };

    const handleSubmit = (e) => {
        e.preventDefault();
        if (formData.brand === '' || formData.model === '' || formData.category === '' || formData.year === null || formData.dateAdded === '' || formData.plaque === '') {
            return;
        }
        setVehicles((prevVehicles) => [...prevVehicles, { ...formData }]);
        setFormData({
            brand: '',
            model: '',
            plaque: '',
            year: '',
            category: '',
            dateAdded: '',
        });
    };

    const handleDelete = (index) => {
        setVehicles((prevVehicles) => {
            const updatedVehicles = [...prevVehicles];
            updatedVehicles.splice(index, 1);
            return updatedVehicles;
        });
    };

    const handleEdit = (index) => {
        const vehicle = vehicles[index];
        setFormData({ ...vehicle });
        handleDelete(index);
    };


    return (
        <>
            <h1 className="text-4xl font-bold text-center text-gray-800 text">

            </h1>
            <div className="flex justify-center items-center">
                <img
                    src={marque}
                    alt="fond1"
                    className="w-auto h-auto font2 mx-auto flex ml-auto"
                />
            </div>

            <div className="flex flex-col md:flex-row">
                <div className="w-full md:w-1/2">
                    <img src={img} alt="fond" className="w-auto h-auto font1 flex ml-auto" />
                </div>

                <div className=" flex items-center justify-center">
                    <div className="flex items-center justify-center px-10 py-52">
                        {/* <div className="flex items-center">
                            <label className="mr-2">Choisissez une option :</label>
                            <select className="border border-gray-300 rounded px-2 py-1">
                                <option value="option1">Option 1</option>
                                <option value="option2">Option 2</option>
                                <option value="option3">Option 3</option>
                            </select>
                        </div> */}

                        <form action="" className='max-w-screen-lg'>
                            <label className="leading-loose">Marque</label>
                            <input
                                type="text"
                                name="brand1"
                                id="brandInput1"
                                // value={}
                                onChange={handleChange}
                                className=" py-2 border focus:ring-gray-500 focus:border-gray-900 w-full sm:text-sm border-gray-300 rounded-md focus:outline-none px-10 text-gray-600"
                                placeholder="Ex: Ferrari"
                            />
                            <div className="pt-4 flex justify-center space-x-4">
                                <button
                                    // onClick={handleAdd}
                                    type="submit"
                                    className=" bg-blue-500 hover:bg-blue-700 w-1/2 bouton font-bold roundedflex justify-center items-center text-white px-4 py-3 rounded-md focus:outline-none"
                                >
                                    Ajouter
                                </button>
                            </div>
                        </form>
                        <form action="" className='max-w-screen-lg py-10 px-10'>
                            <label className="leading-loose">Catégorie</label>
                            <input
                                type="text"
                                name="category1"
                                id="categoryInput1"
                                // value={formData.category}
                                onChange={handleChange}
                                className=" py-2 border focus:ring-gray-500 focus:border-gray-900 w-full sm:text-sm border-gray-300 rounded-md focus:outline-none px-10 text-gray-600"
                                placeholder="Ex: Sport"
                            />
                            <div className="pt-4 flex justify-center space-x-4">
                                <button
                                    // onClick={handleAdd}
                                    type="submit"
                                    className=" bg-blue-500 hover:bg-blue-700 w-1/2 bouton font-bold roundedflex justify-center items-center text-white px-4 py-3 rounded-md focus:outline-none"
                                >
                                    Ajouter
                                </button>
                            </div>
                        </form>
                        <form action="" className='max-w-screen-lg py-10'>
                            <label className="leading-loose">Model</label>
                            <input
                                type="text"
                                name="model1"
                                id="modelInput1"
                                //value={formData.model}
                                onChange={handleChange}
                                className=" py-2 border focus:ring-gray-500 focus:border-gray-900 w-full sm:text-sm border-gray-300 rounded-md focus:outline-none px-10 text-gray-600"
                                placeholder="Ex: Californie"
                            />
                            <div className="pt-4 flex justify-center space-x-4">
                                <button
                                    // onClick={handleAdd}
                                    type="submit"
                                    className=" bg-blue-500 hover:bg-blue-700 w-1/2 bouton font-bold roundedflex justify-center items-center text-white px-4 py-3 rounded-md focus:outline-none"
                                >
                                    Ajouter
                                </button>
                            </div>
                        </form>


                    </div>

                </div>

            </div>
            <div className="flex items-center justify-center">
                <form className="max-w-screen-lg form" onSubmit={handleSubmit}>
                    <div className="min-h-screen flex flex-col justify-center">
                        <div className="relative ">
                            <div className="relative bg-white mx-8 w-full md:mx-0 shadow rounded-3xl sm:p-10">
                                <div className="max-w-md mx-auto ">
                                    <div className="flex items-center space-x-5">

                                        <div className="block pl-2 font-semibold text-xl self-start text-gray-700">
                                            <h2 className="leading-relaxed">AJOUTER UN NOUVEAU VEHICULE</h2>
                                            <p className="text-sm text-gray-500 font-normal leading-relaxed">
                                                Entrez les caractéristiques du véhicule et ajouter.
                                            </p>
                                        </div>
                                    </div>
                                    <div className="divide-y divide-gray-200">
                                        <div className="py-8 text-base leading-6 space-y-4 text-gray-700 sm:text-lg sm:leading-7">
                                            <div className="flex flex-col">
                                                <label className="leading-loose">Marque</label>
                                                <select
                                                    type="text"
                                                    name="brand"
                                                    id="brandInput"
                                                    value={formData.brand}
                                                    onChange={handleChange}
                                                    className="px-4 py-2 border focus:ring-gray-500 focus:border-gray-900 w-full sm:text-sm border-gray-300 rounded-md focus:outline-none text-gray-600"
                                                    placeholder="Ex: Ferrari"
                                                >
                                                    <option value="option1">Lambo</option>
                                <option value="option2">Ferrarie</option>
                                                </select>
                                            </div>
                                            <div className="flex flex-col">
                                                <label className="leading-loose">Modèle</label>
                                                <select
                                                    type="text"
                                                    name="model"
                                                    id="modelInput"
                                                    value={formData.model}
                                                    onChange={handleChange}
                                                    className="px-4 py-2 border focus:ring-gray-500 focus:border-gray-900 w-full sm:text-sm border-gray-300 rounded-md focus:outline-none text-gray-600"
                                                    placeholder="Ex: California"
                                                >
                                                    <option value="option1">Californi</option>
                                <option value="option2">Ruisse</option>
                                                    
                                                  </select>
                                            </div>
                                            <div className="flex flex-col">
                                                <label className="leading-loose">Plaque</label>
                                                <input
                                                    type="text"
                                                    name="plaque"
                                                    id="plaqueInput"
                                                    value={formData.plaque}
                                                    onChange={handleChange}
                                                    className="px-4 py-2 border focus:ring-gray-500 focus:border-gray-900 w-full sm:text-sm border-gray-300 rounded-md focus:outline-none text-gray-600"
                                                    placeholder="Ex: 23E44I"
                                                />
                                             
                                            </div>
                                            <div className="flex flex-col">
                                                <label className="leading-loose">Année</label>
                                                <input
                                                    type="number"
                                                    name="year"
                                                    id="yearInput"
                                                    value={formData.year}
                                                    onChange={handleChange}
                                                    className="px-4 py-2 border focus:ring-gray-500 focus:border-gray-900 w-full sm:text-sm border-gray-300 rounded-md focus:outline-none text-gray-600"
                                                    placeholder="Ex: 2024"
                                                />
                                            </div>
                                            <div className="flex flex-col">
                                                <label className="leading-loose">Catégorie</label>
                                                <select
                                                    type="text"
                                                    name="category"
                                                    id="categoryInput"
                                                    value={formData.category}
                                                    onChange={handleChange}
                                                    className="px-4 py-2 border focus:ring-gray-500 focus:border-gray-900 w-full sm:text-sm border-gray-300 rounded-md focus:outline-none text-gray-600"
                                                    placeholder="Ex: Sport"
                                                >
                                                    <option value="option1">Luxe</option>
                                <option value="option2">Sport</option>
                                                 </select>
                                            </div>
                                            <div className="flex items-center space-x-4">
                                                <div className="flex flex-col">
                                                    <label className="leading-loose">Date dajout</label>
                                                    <div className="relative focus-within:text-gray-600 text-gray-400">
                                                        <input
                                                            type="date"
                                                            name="dateAdded"
                                                            id="dateAddedInput"
                                                            value={formData.dateAdded}
                                                            onChange={handleChange}
                                                            className="pr-4 pl-10 py-2 border focus:ring-gray-500 focus:border-gray-900 w-full sm:text-sm border-gray-300 rounded-md focus:outline-none text-gray-600"
                                                            placeholder="25/02/2020"
                                                        />
                                                        <div className="absolute left-3 top-2">
                                                            <svg
                                                                className="w-6 h-6"
                                                                fill="none"
                                                                stroke="currentColor"
                                                                viewBox="0 0 24 24"
                                                                xmlns="http://www.w3.org/2000/svg"
                                                            >
                                                                <path
                                                                    strokeLinecap="round"
                                                                    strokeLinejoin="round"
                                                                    strokeWidth="2"
                                                                    d="M8 7V3m8 4V3m-9 8h10M5 21h14a2 2 0 002-2V7a2 2 0 00-2-2H5a2 2 0 00-2 2v12a2 2 0 002 2z"
                                                                ></path>
                                                            </svg>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="pt-4 flex items-center space-x-4">
                                            <button
                                                // onClick={handleAdd}
                                                type="submit"
                                                className="bg-blue-500 hover:bg-blue-700 font-bold roundedflex justify-center items-center w-full text-white px-4 py-3 rounded-md focus:outline-none"
                                            >
                                                Ajouter
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>

            {/* Affichage des véhicules ajoutés */}
            <div className='list3 '>
                <h2 className="text-4xl font-semibold text-center text-gray-800 text liste">
                    Liste de vos voitures
                </h2>
                {vehicles && vehicles.length ? '' : <h2 className="text-2xl font-normal text-center text-gray-600 text liste2">
                    Aucune voiture pour linstant 😕!!
                </h2>}
                {vehicles.map((vehicle, index) => (
                    <div key={index} className="my-20 bg-blue-100 shadow rounded-2xl justify-center flex flex-col  px-4 py-10 card">
                        <span className="Number font-bold "> Voiture N°{index + 1}</span>
                        <h3 className="text-lg font-bold mb-2">Marque: <span className="text-xl font-semibold text-gray-600">{vehicle.brand}</span></h3>
                        <h3 className="text-lg font-bold mb-2">Modèle: <span className="text-xl font-semibold text-gray-600">{vehicle.model}</span></h3>
                        <h3 className="text-lg font-bold mb-2">Plaque: <span className="text-xl font-semibold text-gray-600">{vehicle.plaque}</span></h3>
                        <h3 className="text-lg font-bold mb-2">Année: <span className="text-xl font-semibold text-gray-600">{vehicle.year}</span></h3>
                        <h3 className="text-lg font-bold mb-2">Catégorie: <span className="text-xl font-semibold text-gray-600">{vehicle.category}</span></h3>
                        <h3 className="text-lg font-bold mb-2">Date dajout: <span className="text-xl font-semibold text-gray-600">{vehicle.dateAdded}</span></h3>
                        <div className="flex justify-end">
                            <button
                                onClick={() => handleEdit(index)}
                                className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-md mr-2"
                            >
                                Modifier
                            </button>
                            <button
                                onClick={() => handleDelete(index)}
                                className="bg-red-500 hover:bg-red-700 text-white font-bold py-2 px-4 rounded-md"
                            >
                                Supprimer
                            </button>
                        </div>
                    </div>
                ))}
            </div>

        </>
    );
}

export default Form;
